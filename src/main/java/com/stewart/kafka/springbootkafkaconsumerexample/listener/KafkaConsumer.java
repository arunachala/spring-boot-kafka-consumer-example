package com.stewart.kafka.springbootkafkaconsumerexample.listener;

import com.stewart.kafka.springbootkafkaconsumerexample.model.Report;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

//    @KafkaListener(topics = "Report", group = "group_id")
//    public void consume(String message) {
//        System.out.println("Consumed message: " + message);
//    }


    @KafkaListener(topics = "Report", containerFactory = "userKafkaListenerFactory", groupId = "group_id")
    public void consumeJson(Report report) {
        System.out.println("Consumed JSON Message: " + report);
    }
}

