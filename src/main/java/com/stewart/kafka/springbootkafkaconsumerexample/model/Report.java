package com.stewart.kafka.springbootkafkaconsumerexample.model;

public class Report {

    private String email;
    private Object payload;
    private String requester;
    private String template;

    public Report(){

    }

    public Report(String email, Object payload, String requester, String template) {
        this.email = email;
        this.payload = payload;
        this.requester = requester;
        this.template = template;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }

    public String getRequester() {
        return requester;
    }

    public void setRequester(String requester) {
        this.requester = requester;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    @Override
    public String toString() {
        return "Report{" +
                "email='" + email + '\'' +
                ", payload=" + payload +
                ", requester='" + requester + '\'' +
                ", template='" + template + '\'' +
                '}';
    }
}
